
## About

My take on [Kyotu large file reading challenge](https://github.com/Kyotu-Technology/kyotu/tree/main/recruitment-challenges/large-file-reading-challenge).

## Run

Run code using the command:

`mvn spring-boot:run`


## Data

Data are stored in `data/` directory. The  default path for the source of data is in
`data/cities_temperature.csv` but you can change it in `src/main/resources/application.properties` 

## Using

Get the average temepratures for city using endpoint  `localhost:8080/api/temperatures/{cityName}`

For example `localhost:8080/api/teperature/Warszawa`

Polish characters in city name must be [URL-encoded](https://www.urlencoder.org/enc/poland/)

(Example `localhost:8080/api/teperature/Gda%C5%84sk`)