package eu.frezer02.largefilereadingchallenge;

import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import eu.frezer02.largefilereadingchallenge.model.AverageTemperatureInYear;
import eu.frezer02.largefilereadingchallenge.service.CityTemperatureService;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@SpringBootTest
class LargeFileReadingChallengeApplicationTests {

	@Autowired
	private CityTemperatureService cityTemperatureService;

	@Test
	void contextLoads() {
	}

	private void testTemplate(int year, String cityName, float expected) throws IOException {
		Collection<AverageTemperatureInYear> result = cityTemperatureService.getAverageTemperatures(cityName);
		AverageTemperatureInYear averageTemperature =  result.stream().filter( o -> o.getYear() == year).collect(Collectors.toList()).get(0);
		Float epsilon = 0.001f;
		assert Math.abs(averageTemperature.getAverageTemperature() - expected) < epsilon;
	}

	@Test
	public void test1() throws IOException{
		testTemplate(2018, "Wrocław", 16.3701f);
	}

	@Test
	public void test2() throws IOException{
		testTemplate(2022, "Gdańsk",14.7848f);
	}

	@Test
	public void test3() throws IOException{
		testTemplate(2020, "Kraków",15.9107f);
	}

	@Test
	public void test4() throws IOException{
		testTemplate(2019, "Warszawa",13.8074f);
	}


}
