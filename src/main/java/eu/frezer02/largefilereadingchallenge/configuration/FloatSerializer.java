package eu.frezer02.largefilereadingchallenge.configuration;

import java.io.IOException;
import java.text.DecimalFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class FloatSerializer extends JsonSerializer<Float> {

   private static final DecimalFormat df = new DecimalFormat("0.0");

    @Override
    public void serialize(Float arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException {
        if (null == arg0) {
            arg1.writeNull();
        } else {
            arg1.writeNumber(df.format(arg0));
        }
    }
    
}
