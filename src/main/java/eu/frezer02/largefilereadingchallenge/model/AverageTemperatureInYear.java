package eu.frezer02.largefilereadingchallenge.model;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.frezer02.largefilereadingchallenge.configuration.FloatSerializer;
import lombok.Getter;


public class AverageTemperatureInYear {

    @Getter
    private Integer year;

    @Getter
    @JsonSerialize(using = FloatSerializer.class)
    private float averageTemperature;

    private String cityName;

    private Integer numberOfMeassurements;

    private Float sumTemperature;

    public AverageTemperatureInYear(CityTemperature ct) {
        cityName = ct.getCityName();
        sumTemperature = ct.getTemperature();
        averageTemperature = ct.getTemperature();
        numberOfMeassurements = 1;
        year = ct.getYear();
    }

    public synchronized void update(CityTemperature ct) {
        try {
            assert ct.getCityName().equals(cityName);
            assert ct.getYear() == year;
            sumTemperature += ct.getTemperature();
            numberOfMeassurements++;
            averageTemperature = sumTemperature / numberOfMeassurements;
        } catch (Exception e) {
            System.err.println("Failed to update temperature " + e);
        }
    }

}
