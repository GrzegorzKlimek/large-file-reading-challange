package eu.frezer02.largefilereadingchallenge.utils;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import lombok.Getter;

public class TaskTimeWatch {
    private String watchName;
    private Map<String, TaskWatch> tasksMap = Collections.synchronizedMap(new HashMap<>());

    public TaskTimeWatch(String watchName) {
        this.watchName = watchName;
    }

    public void start(String taskName) {
        TaskWatch taskWatch = null;
        if (tasksMap.containsKey(taskName)) {
            taskWatch = tasksMap.get(taskName);
        } else {
            taskWatch = new TaskWatch(taskName);
            tasksMap.put(taskName, taskWatch);
        }
        taskWatch.startTask();
    }

    public void stop(String taskName) {
        if (tasksMap.containsKey(taskName) == false) {
            throw new IllegalArgumentException(
                    String.format("Task %s doesn't exist. So it cannot be stoped", taskName));
        }
        tasksMap.get(taskName).stopTask();
    }

    public String prettyPrint() {
        StringBuilder sb = new StringBuilder();
        Long fullDuration = tasksMap.values().stream().mapToLong(TaskWatch::getDurationMilisecons).sum();
        sb.append(String.format("Task %s. Duration: %s:", watchName, formatDuration(fullDuration)));
        sb.append(System.getProperty("line.separator"));
        for (TaskWatch ts : tasksMap.values()) {

            float percentOfWholeTask = 0f;
            if (ts.getDurationMilisecons() != 0) {
                percentOfWholeTask =  ts.getDurationMilisecons() / (float) fullDuration  * 100;
            }
            sb.append(String.format("   Subtask %s: %.2f%% Duration: %s", ts.name, percentOfWholeTask,
                    formatDuration(ts.durationMilisecons)));
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    public static String formatDuration(long milliseconds) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);

        long remainingMilliseconds = milliseconds - TimeUnit.SECONDS.toMillis(seconds);

        Duration duration = Duration.ofSeconds(seconds, remainingMilliseconds * 1_000_000);

        long days = duration.toDays();
        long hours = duration.toHours() % 24;
        long minutes = duration.toMinutes() % 60;
        long secs = duration.getSeconds() % 60;
        long millis = duration.toMillis() % 1000;

        if (seconds == 0) {
            return String.format("%03d miliseconds", millis);
        } else if (minutes == 0) {
            return String.format("%02d.%03d seconds", secs, millis);
        } else if (hours == 0) {
            return String.format("%02d minutes %02d.%03d seconds", minutes, secs, millis);
        } else if (days == 0) {
            return String.format("%02d hours %02d minutes %02d.%03d seconds", hours, minutes, secs, millis);
        } else {
            return String.format("%d days %02d hours %02d minutes %02d.%03d seconds", days, hours, minutes, secs,
                    millis);
        }

    }

    private class TaskWatch {
        private String name;
        @Getter
        private Long durationMilisecons = 0l;
        private Long lastStart;
        private boolean isRunning = false;

        public TaskWatch(String name) {
            this.name = name;
            this.lastStart = System.currentTimeMillis();
        }

        public void startTask() {
            if (isRunning) {
                throw new IllegalStateException(String.format("Task %s already been started!", name));
            }
            isRunning = true;
            this.lastStart = System.currentTimeMillis();
        }

        public void stopTask() {
            if (isRunning == false) {
                throw new IllegalStateException(
                        String.format("Can't stop task %s because it wasn't started yet", name));
            }
            Long intervalDuration = System.currentTimeMillis() - this.lastStart;
            durationMilisecons += intervalDuration;
            isRunning = false;
        }
    }
}
