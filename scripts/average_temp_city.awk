# Initialize variables
BEGIN {
    count = 0;
    sum = 0;
    found_city = 0;
}

# Process each line
{

    # Check if the second column of the current row starts with "2018"
    if ($1 == city && substr($2, 1, 4) == year) {
        count++;
        sum += $3;
    }
}

# After processing all lines, calculate and print the average
END {
    if (count > 0) {
        average = sum / count;
        print "Average temperature of " city " for year " year " is " average
    } else {
        print "No matching rows found.";
    }
}

