package eu.frezer02.largefilereadingchallenge.model;


import lombok.Getter;
import lombok.Setter;

public class CityTemperature {
    @Getter
    @Setter
    private String cityName;

    @Getter
    @Setter
    private int year;

    @Getter
    @Setter
    private Float temperature;


    public CityTemperature(String line) {
        try {
            String[] words = line.split(";");
            cityName = words[0];
            year = Integer.parseInt(words[1].substring(0,4));
            temperature = Float.parseFloat(words[2]);
        } catch (Exception e) {
            System.err.println(String.format("Error: Invalid source file line: %s. Exception: %s", line, e));
        }
    }

}
