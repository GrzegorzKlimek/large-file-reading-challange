package eu.frezer02.largefilereadingchallenge.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import eu.frezer02.largefilereadingchallenge.model.AverageTemperatureInYear;
import eu.frezer02.largefilereadingchallenge.model.CityTemperature;
import eu.frezer02.largefilereadingchallenge.utils.TaskTimeWatch;

@Service
public class CityTemperatureService {

    @Value("${temperature.source.file.path}")
    private String temperatureSourcePath;

    @Value("${batch.size}")
    private Integer batchSize;

    private AtomicLong lastSourceFileModificationTime = new AtomicLong(0l);

    private Map<String, Map<Integer, AverageTemperatureInYear>> cityAverageTemperatureMap = Collections
            .synchronizedMap(new HashMap<>());

    private void updateTemperatureMap(CityTemperature ct) {
        AverageTemperatureInYear avrTempPerYear = null;
        Integer year = ct.getYear();
        String cityName = ct.getCityName();
        if (cityAverageTemperatureMap.containsKey(cityName) &&
                cityAverageTemperatureMap.get(cityName).containsKey(year)) {
            avrTempPerYear = cityAverageTemperatureMap.get(cityName).get(year);
            avrTempPerYear.update(ct);
        } else {
            avrTempPerYear = new AverageTemperatureInYear(ct);
        }
        if (cityAverageTemperatureMap.containsKey(cityName)) {
            cityAverageTemperatureMap.get(cityName).put(year, avrTempPerYear);
        } else {
            Map<Integer, AverageTemperatureInYear> newCityTemperatureMap = new HashMap<>();
            newCityTemperatureMap.put(year, avrTempPerYear);
            cityAverageTemperatureMap.put(cityName, newCityTemperatureMap);
        }
    }

    private void processLines(List<String> lines, TaskTimeWatch stopWatch) {
        stopWatch.start("Map lines to CityTemperature objescts");
        List<CityTemperature> citiTemperatures = lines.parallelStream().map(CityTemperature::new)
                .collect(Collectors.toList());
        stopWatch.stop("Map lines to CityTemperature objescts");
        stopWatch.start("Update city temperature map");
        citiTemperatures.forEach(this::updateTemperatureMap);
        stopWatch.stop("Update city temperature map");
    }

    private void updateTemperatureInfo(String cityName, TaskTimeWatch stopWatch) throws IOException {
        stopWatch.start("Opening resources");
        BufferedReader bf = null;
        stopWatch.stop("Opening resources");
        File sourceFile = null;
        try {
            sourceFile = new File("./" + temperatureSourcePath);
            bf = new BufferedReader(new FileReader(sourceFile));
            boolean hasNewLine = false;
            List<String> linesToProcess = new ArrayList<>();
            do {
                stopWatch.start("Getting next line");
                String line = bf.readLine();
                stopWatch.stop("Getting next line");
                hasNewLine = line != null;
                if (hasNewLine == false) {
                    processLines(linesToProcess, stopWatch);
                    break;
                } else {
                    linesToProcess.add(line);
                }
                if (linesToProcess.size() >= batchSize) {
                    processLines(linesToProcess, stopWatch);
                    linesToProcess = new ArrayList<>();
                }
            } while (hasNewLine);
        } finally {
            stopWatch.start("Finally block");
            if (bf != null) {
                bf.close();
            }
            if (sourceFile != null) {
                lastSourceFileModificationTime.set(sourceFile.lastModified());
            }
            stopWatch.stop("Finally block");
        }
    }

    private boolean updateNecessary() {
        File file = new File("./" + temperatureSourcePath);
        boolean isUpdateNecessary = lastSourceFileModificationTime.get() != file.lastModified();
        System.out.println("is update necessary " + isUpdateNecessary);
        return isUpdateNecessary;
    }

    public Collection<AverageTemperatureInYear> getAverageTemperatures(String cityName) throws IOException {
        TaskTimeWatch stopWatchGetAverageTemp = new TaskTimeWatch("getAverageTemperatures");
        stopWatchGetAverageTemp.start("getAverageTemperatures");
        if (updateNecessary()) {
            TaskTimeWatch stopWatchUpdateTemp = new TaskTimeWatch("updateTemperatureInfo");
            updateTemperatureInfo(cityName, stopWatchUpdateTemp);
            System.out.println(stopWatchUpdateTemp.prettyPrint());
        }

        if (cityAverageTemperatureMap.containsKey(cityName)) {
            stopWatchGetAverageTemp.stop("getAverageTemperatures");
            System.out.println(stopWatchGetAverageTemp.prettyPrint());
            return cityAverageTemperatureMap.get(cityName).values();
        } else {
            System.out.println("cityAverageTemperatureMap doesn't contains " + cityName);
            System.out.println(cityAverageTemperatureMap.keySet());
        }
        stopWatchGetAverageTemp.stop("getAverageTemperatures");
        System.out.println(stopWatchGetAverageTemp.prettyPrint());
        return new ArrayList<>();
    }

}