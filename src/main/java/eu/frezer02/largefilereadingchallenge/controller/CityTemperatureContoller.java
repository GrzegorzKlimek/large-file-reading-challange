package eu.frezer02.largefilereadingchallenge.controller;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import eu.frezer02.largefilereadingchallenge.model.AverageTemperatureInYear;
import eu.frezer02.largefilereadingchallenge.service.CityTemperatureService;

@RestController
@RequestMapping("/api/teperature/")
public class  CityTemperatureContoller {

    @Autowired
    CityTemperatureService cityTemperatureService;

    @GetMapping("/{cityName}")
    @ResponseBody
    public Collection<AverageTemperatureInYear> getAverageTemperature( @PathVariable("cityName") String cityName) throws IOException {
        return cityTemperatureService.getAverageTemperatures(cityName);
    }
    
}
